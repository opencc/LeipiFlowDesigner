#  雷劈流程设计器
## 资源
1. CCFlow官网：http://ccflow.org
2. 驰骋低代码开发平台：https://ccfast.cc
## 介绍
1. 该系统被驰骋公司收购并接管维护，尊重作者的意愿继续开源。驰骋工作流、表单引擎请访问公司网站。
1. 支持我们请在右上角点Star。
3. 在线演示: http://flowdesign.leipi.org.cn/
4. 该流程设计器被驰骋BPM采用。
4. 界面友好，操作顺畅，代码简洁详细。

## 图片展示

![![输入图片说明](https://images.gitee.com/uploads/images/2021/0311/101639_aa9335d5_358162.gif "aa.gif")](http://flowdesign.leipi.org.cn/Public/images/100004709774201.gif "在这里输入图片标题")


## 静态展示:

![输入图片说明](https://images.gitee.com/uploads/images/2021/0311/094928_7612c904_358162.png "屏幕截图.png")


## 国内最容易使用和开发的流程设计器

1. 界面简洁，容易操作，用户体验好
1. 支持子流程
1. 转出条件
1. 步骤权限控制
1. 结合表单设计器
1. 步骤独立样式
1. 会签控制

## 软件架构

1. 纯粹js代码
2. jquery.multiselect2side
3. jquery-ui


## 安装教程

1. 我们提供了一个php安装demo，下载后请参考php代码。
2. 您可以独立的使用纯粹js运行设计器。
3. 节点，连接线您可以自己保存到数据库并展现。

# 关于驰骋公司

## 开源产品
1. 驰骋公司是一家开源软件公司，官方网站：http://ccflow.org
2. 代表开源作品：.net/java 版本的CCFlow工作流引擎，表单引擎，支持多种数据库。
3. 下载地址： http://ccflow.org/down.htm
3. 视频教程：http://ccflow.org/ke.htm

## 提供的服务

1. 流程引擎技术、表单引擎技术的支持服务。
2. 培训、授权、二次开发、协助项目上线服务。


